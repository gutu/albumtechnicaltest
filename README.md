This project is a Android application wich list albums from https://img8.leboncoin.fr/technical-test.json.
The project is coded on Kotlin. 
- minimum SDK 15

Libraries : 

Basics from android.support:
     "com.android.support:cardview-v7:27.1.1"
     'com.android.support:recyclerview-v7:27.1.1'
     "com.android.support:design:27.1.1"

For HTTP connections: 
     'com.squareup.okhttp3:logging-interceptor:3.9.1'
     "com.squareup.retrofit2:converter-gson:2.4.0"
     "com.squareup.retrofit2:adapter-rxjava2:2.4.0"
Picasso for pictures :   'com.squareup.picasso:picasso:2.5.2'

RxJava to performing a network call:
    "io.reactivex.rxjava2:rxandroid:2.0.2"
    'io.reactivex.rxjava2:rxjava:2.1.13'
    
Stetho to see local database and network calls (used with Chrome/inspect/#devices)
    'com.facebook.stetho:stetho:1.5.0'
    'com.facebook.stetho:stetho-okhttp3:1.5.0'

Use of ViewModel Architecture :
 "android.arch.lifecycle:extensions:1.1.1"
 
DBFlow to create a local database : 
     "com.github.Raizlabs.DBFlow:dbflow-core:4.2.4"
     "com.github.Raizlabs.DBFlow:dbflow:4.2.4"
     "com.github.Raizlabs.DBFlow:dbflow-kotlinextensions:4.2.4"