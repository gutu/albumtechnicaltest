package com.cgutu.albums.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.cgutu.albums.R
import com.cgutu.albums.manager.AlbumsManager
import com.cgutu.albums.view.AlbumsView
import com.cgutu.albums.viewmodel.AlbumsViewModel
import com.cgutu.albums.viewmodel.BaseAlbumsViewModelFactory
import kotlin.properties.Delegates

class AlbumsFragment : Fragment() {

	private var albumsView by Delegates.notNull<AlbumsView>()
	private val viewModel by lazy {
		ViewModelProviders.of(this, BaseAlbumsViewModelFactory(activity!!.application,
				AlbumsManager(activity!!.applicationContext)))
				.get(AlbumsViewModel::class.java)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
	                          savedInstanceState: Bundle?): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		val rootView = inflater.inflate(R.layout.fragment_albums, container, false)
		albumsView = AlbumsView(rootView, viewModel)
		return rootView
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
	}

	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.main_menu, menu)
		super.onCreateOptionsMenu(menu, inflater)
	}

	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		return when (item?.itemId) {
			R.id.action_viewType -> {
				albumsView.updateRecyclerView(!albumsView.isGrid)
				if (albumsView.isGrid) {
					item.setIcon(R.drawable.ic_view_list)
				} else {
					item.setIcon(R.drawable.ic_view)
				}
				return true
			}
			else                 -> super.onOptionsItemSelected(item)
		}
	}

	override fun onDestroy() {
		super.onDestroy()
		albumsView.release()
	}
}