package com.cgutu.albums

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.cgutu.albums.core.inTransaction
import com.cgutu.albums.fragment.AlbumsFragment

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		supportFragmentManager.inTransaction {
			add(R.id.contentFrame, AlbumsFragment())
		}

	}
}
