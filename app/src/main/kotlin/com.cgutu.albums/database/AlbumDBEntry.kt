package com.cgutu.albums.database

import com.cgutu.albums.data.Album
import com.cgutu.albums.database.AlbumDBEntry.Companion.NAME
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel

@Table(name = NAME, database = AppDatabase::class, allFields = true)
class AlbumDBEntry : BaseModel {
	companion object {
		const val NAME = "AlbumDBEntry"
	}

	@PrimaryKey(autoincrement = true)
	@Column
	var id: Int = 0
	@Column
	var title: String = ""
	@Column
	var url: String = ""
	@Column
	var thumbnailUrl: String = ""

	constructor() {
		// Mandatory for DBFlow
	}

	constructor(album: Album) {
		album.let {
			this.id = it.id
			this.title = it.title
			this.url = it.url
			this.thumbnailUrl = it.thumbnailUrl
		}
	}

	fun toDomain(): Album = Album(id, title, url, thumbnailUrl)
}
