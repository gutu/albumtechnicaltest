package com.cgutu.albums.database

import com.raizlabs.android.dbflow.annotation.Database

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION, generatedClassSeparator = "_")
object AppDatabase {
	const val NAME: String = "AlbumsDatabase"
	const val VERSION: Int = 1
}