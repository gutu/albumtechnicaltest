package com.cgutu.albums.data

import java.io.Serializable

data class Album(val id: Int, val title: String, val url: String, val thumbnailUrl: String) :
		Serializable {}
