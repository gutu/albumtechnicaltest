package com.cgutu.albums.ui.data

data class AlbumUiData<T>(val state: State = State.IDLE, val data: T, val error: String? = null)

enum class State {
	/**
	 * state refreshing stand for the loading when the screen is not empty
	 */
	LOADING,
	REFRESHING, IDLE, EMPTY, ERROR, SUCCESS
}