package com.cgutu.albums.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.cgutu.albums.R
import com.cgutu.albums.core.bind
import com.cgutu.albums.core.loadImage
import com.cgutu.albums.data.Album

class AlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

	private val title by itemView.bind<TextView>(R.id.titleView)
	private val picture by itemView.bind<ImageView>(R.id.pictureView)

	fun bind(data: Album) {
		title.text = data.title.capitalize()
		picture.loadImage(data.url)
	}
}