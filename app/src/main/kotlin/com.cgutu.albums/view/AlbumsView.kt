package com.cgutu.albums.view

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.cgutu.albums.R
import com.cgutu.albums.adapter.AlbumsAdapter
import com.cgutu.albums.core.bind
import com.cgutu.albums.ui.data.State
import com.cgutu.albums.viewmodel.AlbumsViewModel
import io.reactivex.disposables.CompositeDisposable

private const val SPAN = 2

class AlbumsView(private val rootView: View, private val viewModel: AlbumsViewModel) {

	private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
	private val recyclerViewAlbums by rootView.bind<RecyclerView>(R.id.recyclerView)
	private val albumsAdapter by lazy { AlbumsAdapter() }
	private val swipeRefreshLayout by rootView.bind<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
	private val loading by rootView.bind<View>(R.id.loading)
	private val emptyView by rootView.bind<View>(R.id.emptyView)
	var isGrid: Boolean = true

	init {
		updateRecyclerView(isGrid)
		swipeRefreshLayout.setOnRefreshListener { viewModel.refresh() }
		compositeDisposable.add(viewModel.albumUiDataSource.subscribe { it ->
			albumsAdapter.submitList(it.data)
			updateScreenState(it.state)
		})
	}

	fun updateRecyclerView(result: Boolean) {
		isGrid = result
		albumsAdapter.isGrid = result
		if (result) {
			recyclerViewAlbums.layoutManager = GridLayoutManager(rootView.context, SPAN)
		} else {
			recyclerViewAlbums.layoutManager = LinearLayoutManager(rootView.context)
		}
		recyclerViewAlbums.adapter = albumsAdapter
	}

	private fun updateScreenState(state: State) {
		when (state) {
			State.LOADING                          -> {
				loading.visibility = View.VISIBLE
				emptyView.visibility = View.GONE
				setSwipeRefreshLayoutState(false)
			}
			State.REFRESHING                       -> {
				loading.visibility = View.GONE
				emptyView.visibility = View.GONE
				setSwipeRefreshLayoutState(true)
			}
			State.IDLE, State.SUCCESS, State.ERROR -> {
				loading.visibility = View.GONE
				emptyView.visibility = View.GONE
				setSwipeRefreshLayoutState(false)
			}
			State.EMPTY                            -> {
				loading.visibility = View.GONE
				emptyView.visibility = View.VISIBLE
				setSwipeRefreshLayoutState(false)
			}
		}
	}

	private fun setSwipeRefreshLayoutState(isRefreshing: Boolean) {
		if (swipeRefreshLayout.isRefreshing) {
			swipeRefreshLayout.isRefreshing = isRefreshing
		}
	}

	fun release() {
		compositeDisposable.clear()
	}
}