package com.cgutu.albums.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cgutu.albums.R
import com.cgutu.albums.data.Album
import com.cgutu.albums.view.AlbumViewHolder

class AlbumsAdapter : ListAdapter<Album, AlbumViewHolder>(diffCallback) {

	var isGrid: Boolean = true

	companion object {
		const val LIST_VIEW: Int = 0
		const val GRID_VIEW: Int = 1

		private val diffCallback = object : DiffUtil.ItemCallback<Album>() {
			override fun areItemsTheSame(oldItem: Album?, newItem: Album?): Boolean {
				if (oldItem != null && newItem != null) {
					return oldItem.id == newItem.id
				}
				return false
			}

			override fun areContentsTheSame(oldItem: Album?, newItem: Album?): Boolean {
				if (oldItem != null && newItem != null) {
					return oldItem.title == newItem.title
				}
				return false
			}
		}
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
		var view = if (LIST_VIEW == viewType) {
			LayoutInflater.from(parent.context)
					.inflate(R.layout.viewholder_album, parent, false)
		} else {
			LayoutInflater.from(parent.context)
					.inflate(R.layout.viewholder_album_columns, parent, false)
		}
		return AlbumViewHolder(view)
	}

	override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
		holder.bind(getItem(position))
	}

	override fun getItemViewType(position: Int): Int {
		return if (isGrid) {
			GRID_VIEW
		} else {
			LIST_VIEW
		}
	}
}