package com.cgutu.albums.viewmodel

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.cgutu.albums.manager.AlbumsManager

class BaseAlbumsViewModelFactory(private val context: Application, private val albumsManager: AlbumsManager) :
		ViewModelProvider.NewInstanceFactory() {

	override fun <T : ViewModel?> create(modelClass: Class<T>): T {
		if (modelClass.isAssignableFrom(AlbumsViewModel::class.java)) {
			return AlbumsViewModel(context, albumsManager) as T
		}
		throw IllegalArgumentException("This factory handle only AlbumsViewModel classes")
	}
}