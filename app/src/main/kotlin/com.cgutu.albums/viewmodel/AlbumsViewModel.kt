package com.cgutu.albums.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.util.Log
import com.cgutu.albums.data.Album
import com.cgutu.albums.manager.AlbumsManager
import com.cgutu.albums.ui.data.AlbumUiData
import com.cgutu.albums.ui.data.State
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers

private const val TAG = "AlbumsViewModel"

class AlbumsViewModel(context: Application, private val albumsManager: AlbumsManager) :
		AndroidViewModel(context) {

	private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
	val albumUiDataSource: BehaviorProcessor<AlbumUiData<List<Album>>> by lazy {
		BehaviorProcessor.createDefault<AlbumUiData<List<Album>>>(
				AlbumUiData(State.LOADING, emptyList()))
	}

	init {
		fetchAlbums()
	}

	fun fetchAlbums() {
		compositeDisposable.add(albumsManager.getAlbums().subscribeOn(Schedulers.io()).observeOn(
				AndroidSchedulers.mainThread()).subscribe({ albums ->
			Log.e(TAG, "albums " + albums.size)
			val state = if (albums.isEmpty()) {
				State.EMPTY
			} else {
				State.IDLE
			}
			albumUiDataSource.onNext(AlbumUiData(state, albums))
		}, { throwable ->
			Log.e(TAG, "Failed to get albums", throwable)
			albumUiDataSource.onNext(AlbumUiData(State.EMPTY, emptyList()))
		}))
	}

	fun refresh() {
		albumUiDataSource.value?.let {
			val state = if (it.data.isEmpty()) {
				State.LOADING
			} else {
				State.REFRESHING
			}
			albumUiDataSource.onNext(it.copy(state))
		}
		fetchAlbums()
	}

	override fun onCleared() {
		super.onCleared()
		compositeDisposable.clear()
	}
}