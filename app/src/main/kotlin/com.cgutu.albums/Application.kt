package com.cgutu.albums

import android.app.Application
import com.facebook.stetho.Stetho
import com.raizlabs.android.dbflow.config.FlowManager

class Application : Application() {
	override fun onCreate() {
		super.onCreate()
		Stetho.initializeWithDefaults(this)
		FlowManager.init(this)
	}

	override fun onTerminate() {
		super.onTerminate()
		FlowManager.destroy()
	}
}