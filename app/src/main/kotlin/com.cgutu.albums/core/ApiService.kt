package com.cgutu.albums.core

import android.content.Context
import com.cgutu.albums.R
import com.cgutu.albums.data.Album
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface ApiService {

	@GET("/technical-test.json")
	fun getAllAlbums(): Single<List<Album>>

	companion object {
		fun create(context: Context): ApiService {
			val retrofit = Retrofit.Builder()
					.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
					.addConverterFactory(GsonConverterFactory.create())
					.baseUrl(context.getString(R.string.configurationUrl))
					.build()

			return retrofit.create(ApiService::class.java)
		}
	}
}