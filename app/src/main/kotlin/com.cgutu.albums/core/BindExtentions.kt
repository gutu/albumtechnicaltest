package com.cgutu.albums.core

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.View

fun <T : View> FragmentActivity.bind(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
}

fun <T : View?> FragmentActivity.bindOptional(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
}

fun <T : View> View.bind(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
}

fun <T : View?> View.bindOptional(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
}

fun <T : View> Fragment.bind(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { view!!.findViewById<T>(res) }
}

fun <T : View?> Fragment.bindOptional(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { view!!.findViewById<T>(res) }
}

fun <T : View> RecyclerView.ViewHolder.bind(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { itemView!!.findViewById<T>(res) }
}

fun <T : View?> RecyclerView.ViewHolder.bindOptional(@IdRes res: Int): Lazy<T> {
	return lazy(LazyThreadSafetyMode.NONE) { itemView!!.findViewById<T>(res) }
}
