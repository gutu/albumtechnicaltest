package com.cgutu.albums.core

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.cgutu.albums.R
import com.squareup.picasso.Picasso


fun ImageView.loadImage(url: String?, @DrawableRes placeholder: Int = R.drawable.ic_placeholder) {
	if (url == null || url.isEmpty()) {
		setImageResource(placeholder)
	} else {
		val picasso = Picasso.with(context)
		picasso.load(url)
				.placeholder(placeholder)
				.error(placeholder)
				.into(this)
	}
}