package com.cgutu.albums.manager

import android.content.Context
import com.cgutu.albums.core.ApiService
import com.cgutu.albums.data.Album
import com.cgutu.albums.database.AlbumDBEntry
import com.cgutu.albums.database.AppDatabase
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.sql.language.SQLite
import io.reactivex.Single
import kotlin.properties.Delegates

class AlbumsManager(context: Context) {

	private var apiService by Delegates.notNull<ApiService>()

	init {
		apiService = ApiService.create(context)
	}

	fun getAlbums(): Single<List<Album>> {
		return Single.fromCallable<List<Album>> {
			retrieveAlbums()
		}
				.filter { albums -> !albums.isEmpty() }
				.switchIfEmpty(fetchAlbums())

	}

	private fun fetchAlbums(): Single<List<Album>> {
		return apiService.getAllAlbums()
				.map { results ->
					clean()
					insertAlbums(results)
					results
				}
	}

	private fun insertAlbums(albums: List<Album>) {
		FlowManager.getDatabase(AppDatabase.javaClass)
				.beginTransactionAsync {
					albums.forEach { album ->
						AlbumDBEntry(album).save()
					}
				}
				.build()
				.execute()
	}

	private fun retrieveAlbums(): List<Album>? {
		return SQLite.select()
				.from(AlbumDBEntry::class.java)
				.queryList()
				.map { it.toDomain() }
	}

	private fun clean() {
		SQLite.delete(AlbumDBEntry::class.java)
				.async()
				.execute()
	}
}